package com.risac.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.risac.entities.PersistedEntity;

public interface PersistedEntityRepository<T extends PersistedEntity, ID> extends JpaRepository<T, ID> {

}
