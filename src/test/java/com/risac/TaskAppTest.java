package com.risac;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.risac.JPAServices.BetService;
import com.risac.entities.bet.Bet;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TaskAppTest {

	private Bet bet;

	@Autowired
	BetService betService;

	@BeforeEach
	public void setup() {

		bet = new Bet();

		bet.setClientid(1001L);
		bet.setGame("Russian Roulette");
		bet.setNumbets(6);
		bet.setStake(5);
		bet.setReturns(1);
		LocalDate localDate = LocalDate.of(2022, 2, 19);
		new Date(0);
		bet.setDate(Date.valueOf(localDate));

	}

	@Test
	@DirtiesContext
	public void goTest() {

		betService.save(bet);

		assertTrue(betService.existsById(bet.getId()));

		assertFalse("roulette".equals(bet.getGame()));
		assertTrue("Russian Roulette".equals(bet.getGame()));
		assertEquals(6, bet.getNumbets());
		assertEquals(5, bet.getStake());
		assertEquals(1, bet.getReturns());
		assertEquals(1001L, bet.getClientid());

		String testDate = LocalDate.of(2022, 02, 19).toString();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String actualDate = formatter.format(bet.getDate());
		assertEquals(testDate, actualDate);

		betService.deleteById(bet.getId());
		assertFalse(betService.existsById(bet.getId()));

	}

}
