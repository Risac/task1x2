package com.risac.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

@Configuration
public class KafkaTopicConfig {

	@Value(value = "${spring.kafka.producer.bootstrap-servers}")
	private String bootstrapAddress;

	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configs = new HashMap<>();
		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		return new KafkaAdmin(configs);
	}

	/*
	 * Topic bet_detail
	 */
	@Bean
	public NewTopic topicBetDetail() {
		return new NewTopic("bet_detail", 1, (short) 1);
	}

	/*
	 * Topic some_other_topic - NOT USED
	 */
	@Bean
	public NewTopic topicOther() {
		return new NewTopic("some_other_topic", 1, (short) 1);
	}
}