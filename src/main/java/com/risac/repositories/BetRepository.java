package com.risac.repositories;

import com.risac.entities.bet.Bet;

public interface BetRepository extends PersistedEntityRepository<Bet, Long> {

}
