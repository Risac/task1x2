package com.risac;

import java.io.FileNotFoundException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.risac.JPAServices.BetService;
import com.risac.entities.bet.Bet;
import com.risac.kafka.KafkaController;
import com.risac.services.ReadJson;

@Component
public class TaskApp {

	@Resource
	BetService betService;

	@Autowired
	KafkaController kafkaController;

	private static final Logger logger = LogManager.getLogger(TaskApp.class.getName());

	public void go() {
		String filePath = "src\\main\\resources\\JSON.json";
		// Read JSON from file
		List<Bet> bets = null;
		try {
			bets = ReadJson.getBetsFromFile(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Save each bet
		for (Bet bet : bets) {
			betService.save(bet);

			boolean exists = betService.existsById(bet.getId());
			if (exists) { // check if Bet is in the base before sending message
				kafkaController.sendBetMessage(bet);
			}
		}

		/* Quick stream for saving Bets - NOT USED */
//		bets.stream().forEach(b -> {
//			betService.save(b);
//			kafkaController.sendBetMessage(b);
//
//		});
	}

	/*
	 * Process received kafka message containing Bet
	 */
	@KafkaListener(topics = "bet_detail", containerFactory = "betKafkaListenerContainerFactory")
	public void betListener(Bet bet) {

		logger.log(Level.INFO, "Bet: " + bet.getId() + " successfully saved.");

		if (bet.getReturns() >= 1500) {

			logger.log(Level.WARN, "Bet: " + bet.getId() + " with customerId: " + bet.getClientid()
					+ " has returns of: " + bet.getReturns() + ", throw him out!");

		}

	}

	// Listens to console inputs
	@KafkaListener(topics = "some_other_topic", containerFactory = "kafkaListenerContainerFactory")
	public void betListener(String msg) {
		System.out.println(msg);

		logger.log(Level.INFO, msg);

	}

}
