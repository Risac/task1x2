package com.risac.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestParam;

import com.risac.entities.bet.Bet;

@org.springframework.stereotype.Controller
public class KafkaController {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	private KafkaTemplate<String, Bet> betKafkaTemplate;

	/*
	 * Send Bet message
	 */
	public void sendBetMessage(@RequestParam Bet bet) {
		betKafkaTemplate.send("bet_detail", bet);
	}

	/*
	 * Send String message
	 */
	public void sendMessage(@RequestParam String msg) {
		kafkaTemplate.send("some_other_topic", msg);
	}

}