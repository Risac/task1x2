package com.risac;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class JSPCodeTest {

	@Test
	public void testJspQueryBuilder() {

		String clientId = null;
		String game = null;
		String date = null;
		StringBuilder testCode1 = jspStringBuilder(clientId, game, date);
		assertTrue("SELECT * FROM BET".equals(testCode1.toString()));

		clientId = "2";
		game = "baccarat";
		date = "2020-05-17";
		StringBuilder testCode2 = jspStringBuilder(clientId, game, date);
		assertTrue("SELECT * FROM BET WHERE clientId= 2 AND game= 'baccarat' AND date= '2020-05-17'"
				.equals(testCode2.toString()));

		clientId = "1";
		game = "roulette";
		date = "2020-05-16";
		StringBuilder testCode3 = jspStringBuilder(clientId, game, date);
		assertTrue("SELECT * FROM BET WHERE clientId= 1 AND game= 'roulette' AND date= '2020-05-16'"
				.equals(testCode3.toString()));

		clientId = null;
		game = "blackjack";
		date = null;
		StringBuilder testCode4 = jspStringBuilder(clientId, game, date);
		assertTrue("SELECT * FROM BET".equals(testCode4.toString()));

		clientId = "1";
		game = "";
		date = "";
		StringBuilder testCode5 = jspStringBuilder(clientId, game, date);
		assertTrue("SELECT * FROM BET WHERE clientId= 1".equals(testCode5.toString()));

		clientId = "";
		game = "";
		date = "2022-02-19";
		StringBuilder testCode6 = jspStringBuilder(clientId, game, date);
		assertTrue("SELECT * FROM BET WHERE date= '2022-02-19'".equals(testCode6.toString()));

	}

	// Code from jsp page.
	private StringBuilder jspStringBuilder(String clientId, String game, String date) {

		StringBuilder query = new StringBuilder();
		query.append("SELECT * FROM BET");

		boolean firstParam = true;

		// Check if there are any sent paramters
		if (clientId != null && game != null && date != null) {
			// If there are parameters, extend query
			if (!clientId.isEmpty() || !game.isEmpty() || !date.isEmpty()) {
				query.append(" WHERE ");

			}
		}

		if (clientId != null && game != null && date != null) { // null check for fields
			// Check parameter clientID
			if (!clientId.isEmpty()) {
				if (!firstParam) { // If it's not first parameter
					query.append(" AND ");
				}
				query.append("clientId= " + clientId);

				firstParam = false;
			}

			if (!game.isEmpty()) {
				if (!firstParam) {
					query.append(" AND ");
				}
				query.append("game= " + "\'" + game + "\'");

				firstParam = false;
			}

			if (!date.isEmpty()) {
				if (!firstParam) {
					query.append(" AND ");
				}
				query.append("date= " + "\'" + date + "\'");

				firstParam = false;
			}
		}
		return query;
	}

}
