package com.risac.repositories;

import com.risac.entities.client.Client;

public interface ClientRepository extends PersistedEntityRepository<Client, Long> {

}
