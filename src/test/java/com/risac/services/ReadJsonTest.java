package com.risac.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.risac.entities.bet.Bet;

@SpringBootTest
public class ReadJsonTest {

	@Test
	public void getBetsFromFile() {

		String filePath = "src\\main\\resources\\JSON.json";

		List<Bet> list = null;
		try {
			list = ReadJson.getBetsFromFile(filePath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		assertEquals(1, list.get(0).getClientid());

		assertEquals("roulette", list.get(0).getGame());
		assertFalse(list.get(0).getGame().equals("baccarate"));

		assertEquals(5.0, list.get(0).getNumbets(), 1);
		assertEquals(100.0, list.get(0).getStake(), 1);
		assertEquals(0.0, list.get(0).getReturns(), 1);

		String testDate = LocalDate.of(2020, 5, 17).toString();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String actualDate = formatter.format(list.get(0).getDate());
		assertEquals(testDate, actualDate);

		filePath = "wrong path";
		try {
			list = ReadJson.getBetsFromFile(filePath);
		} catch (FileNotFoundException e) {
			assertTrue("wrong path (The system cannot find the file specified)".equals(e.getMessage()));
		}

	}

}
