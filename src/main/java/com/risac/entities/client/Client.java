package com.risac.entities.client;

import javax.persistence.Entity;

import com.risac.entities.PersistedEntity;

@Entity
public class Client extends PersistedEntity {

	private Client() {
		super();
	}

	@Override
	public String toString() {
		return "Client [getId()=" + getId() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + "]";
	}

}
