FROM openjdk:16
ADD target/task1x2.jar task1x2.jar
EXPOSE 8080
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "task1x2.jar"]