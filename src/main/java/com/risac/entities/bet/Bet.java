package com.risac.entities.bet;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import com.risac.entities.PersistedEntity;

@Entity
@Table(indexes = { @Index(name = "clid_index", columnList = "clientId"),
		@Index(name = "date_index", columnList = "date"), @Index(name = "game_index", columnList = "game") })
public class Bet extends PersistedEntity implements Serializable {

	private int numbets;

	@Column(length = 55)
	private String game;

	private double stake;
	private double returns;
	private long clientid;
	private Date date;

	// Hibernate requires default constructor
	public Bet() {

	}

	public Bet(int numbets, String game, double stake, double returns, long clientid, Date date) {
		super();
		this.numbets = numbets;
		this.game = game;
		this.stake = stake;
		this.returns = returns;
		this.clientid = clientid;
		this.date = date;
	}

	public int getNumbets() {
		return numbets;
	}

	public void setNumbets(int numbets) {
		this.numbets = numbets;
	}

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public double getStake() {
		return stake;
	}

	public void setStake(double stake) {
		this.stake = stake;
	}

	public double getReturns() {
		return returns;
	}

	public void setReturns(double returns) {
		this.returns = returns;
	}

	public long getClientid() {
		return clientid;
	}

	public void setClientid(long clientid) {
		this.clientid = clientid;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(clientid, date, game, numbets, returns, stake);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Bet other = (Bet) obj;
		return clientid == other.clientid && Objects.equals(date, other.date) && Objects.equals(game, other.game)
				&& numbets == other.numbets
				&& Double.doubleToLongBits(returns) == Double.doubleToLongBits(other.returns)
				&& Double.doubleToLongBits(stake) == Double.doubleToLongBits(other.stake);
	}

	@Override
	public String toString() {
		return "Bet [numbets=" + numbets + ", game=" + game + ", stake=" + stake + ", returns=" + returns
				+ ", clientid=" + clientid + ", date=" + date + "]";
	}

}
