package com.risac;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Task1x2Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Task1x2Application.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(Task1x2Application.class);
	}

	@Bean
	public CommandLineRunner demo(TaskApp app) {
		return (args) -> {
			app.go();
		};
	}

}
