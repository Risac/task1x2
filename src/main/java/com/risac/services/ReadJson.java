package com.risac.services;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.risac.entities.bet.Bet;

/**
 * Reads in JSON provided in file.
 */
public class ReadJson {

	/**
	 * Parsing a JSON file.
	 *
	 * @return List of Bets
	 */
	public static List<Bet> getBetsFromFile(String filePath) throws FileNotFoundException {

		List<Bet> listOfBets = new ArrayList<>();

		try (FileReader reader = new FileReader(filePath)) {

			// JsonParser jsonParser = new JsonParser(); // deprecated, use as static
			JsonObject jo = (JsonObject) JsonParser.parseReader(reader);
			JsonArray jsonArr = jo.getAsJsonArray("bets");

			Gson googleJson = new Gson();

			// find exact type
			Type userListType = new TypeToken<List<Bet>>() {
			}.getType();

			listOfBets = googleJson.fromJson(jsonArr, userListType);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfBets;
	}

	/**
	 * Parsing from API.
	 *
	 * @return List of Bets
	 */
	public static List<Bet> getBetsFromAPI(String apiURL) {

		List<Bet> listOfBets = new ArrayList<>();
		Gson googleJson = new Gson();

		try {
			URL url = new URL(apiURL);
			URLConnection urlConnection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			listOfBets = Arrays.asList(googleJson.fromJson(bufferedReader, Bet.class));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfBets;
	}
}
